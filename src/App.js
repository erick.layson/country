import React, { useState, useEffect } from 'react';
import axios from 'axios';

import MaterialTable from 'material-table';

import { ThemeProvider, createTheme } from '@mui/material';

const App = () => {

  
        const [data, setData] = useState([]);

        const columns = [
          {title:"Name",field:"name"},
          {title:"Region",field:"region"},
          {title:"Area",field:"area"}
        ]

        useEffect(()=>{
          fetch("https://restcountries.com/v2/all?")
          .then(res=>res.json())
          .then(res=>setData(res))
        },[])

        const defaultMaterialTheme = createTheme();
        
        return(
          <div className="App">
            <div style={{ width: '100%', height: '100%' }}>
                <ThemeProvider theme={defaultMaterialTheme}>
                          <MaterialTable
                                title="Countries"
                                data={data}
                                columns={columns}
                                
                              />
                </ThemeProvider>
            </div>        
          </div>
        );
  }

  export default App;